package Pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (isNotPossible(inputNumbers)){
            throw new CannotBuildPyramidException();
        }

        inputNumbers.sort(Integer::compareTo);
        int rowAmount = countAmountOfRows(inputNumbers);
        int colAmount = countAmountOfCols(rowAmount);

        int[][] pyramid = new int[rowAmount][colAmount];

        int pos = 0;
        for (int row = 0; row < rowAmount; row++) {
            int colStart = rowAmount - row - 1;
            for (int elemNum = 0; elemNum <= row; elemNum++) {
                Integer number = inputNumbers.get(pos);
                pyramid[row][colStart + elemNum * 2] = number;
                pos++;
            }
        }


        return pyramid;
    }

    private int countAmountOfCols(final int rowAmount) {
        return rowAmount + rowAmount - 1;
    }

    private boolean isNotPossible(final List<Integer> inputNumbers) {
        return containsNull(inputNumbers) || wrongAmount(inputNumbers);
    }

    private boolean wrongAmount(final List<Integer> inputNumbers) {
        return countAmountOfRows(inputNumbers) == -1;
    }

    private boolean containsNull(final List<Integer> inputNumbers) {
        for (Integer i : inputNumbers) {
            if (i == null) return true;
        }
        return false;
    }

    private int countAmountOfRows(final List<Integer> inputNumbers){
        int amount = inputNumbers.size();
        int elemsInTheRow = 0;
        while (amount > 0){
            elemsInTheRow++;
            amount -= elemsInTheRow;
        }
        return amount == 0 ? elemsInTheRow : -1;
    }
}
