package Calculator;


public class CalculatorWorker {

    private String line;
    private int lineEnd;
    private int pos;

    public CalculatorWorker(final String line) {
        this.line = line;
    }

    int roundConst = 100000;

    public String work() {
        try {
            lineEnd = line.length();
            double result = parseExpression();
            if (pos != lineEnd) {
                return null;
            }

            return getPresentationOfResult(result);
        } catch (Exception e) {
            return null;
        }
    }

    private String getPresentationOfResult(double result) {
        result *= roundConst;
        result = Math.round(result);
        if (result % roundConst == 0) {
            return Integer.toString((int) (result / roundConst));
        }
        result = result / roundConst;
        return Double.toString(result);

    }

    private double parseExpression() {
        double term1 = parseTerm();
        while (pos < lineEnd) {
            char cur = line.charAt(pos);
            if (cur == '+' || cur == '-') {
                char symbol = line.charAt(pos++);
                double term2 = parseTerm();

                if (symbol == '+') {
                    term1 += term2;
                } else {
                    term1 -= term2;
                }
            } else {
                break;
            }
        }
        return term1;
    }

    private double parseTerm() {
        double factor1 = parseFactor();
        while (pos < lineEnd) {
            char cur = line.charAt(pos);
            if (cur == '*' || cur == '/') {
                pos++;
                double factor2 = parseFactor();
                if (cur == '*') {
                    factor1 *= factor2;
                } else {
                    if (factor2 == 0) {
                        throw new IllegalArgumentException();
                    }
                    factor1 /= factor2;
                }
            } else {
                break;
            }
        }
        return factor1;
    }

    private double parseFactor() {
        if (line.charAt(pos) == '(') {
            pos++;
            double number = parseExpression();
            pos++;
            return number;
        } else {
            return parseNumber();
        }
    }

    private double parseNumber() {
        StringBuilder number = new StringBuilder();
        String partBeforeDot = readNumberPart();
        number.append(partBeforeDot);
        if (pos < lineEnd) {
            char cur = line.charAt(pos);
            if (cur == '.') {
                pos++;
                number.append('.');
                String partAfterDot = readNumberPart();
                number.append(partAfterDot);
            }
        }
        return Double.parseDouble(number.toString());
    }

    private String readNumberPart() {
        StringBuilder number = new StringBuilder();
        char cur = line.charAt(pos);
        while (Character.isDigit(cur) && pos < lineEnd) {
            number.append(cur);
            pos++;
            if (pos < lineEnd) {
                cur = line.charAt(pos);
            }
        }
        return number.toString();
    }

}
