package Subsequence;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null){
            throw new IllegalArgumentException();
        }
        int subsequenceSize = x.size();

        // Remove all elements from y for getting required subsequence
        if (subsequenceSize == 0){
            return true;
        }

        Deque subsequence = new ArrayDeque(x);
        for (Object obj : y) {
            if (subsequence.isEmpty()){
                break;
            }
            Object cur = subsequence.peekFirst();
            if (cur.equals(obj)){
                subsequence.pollFirst();
            }
        }

        return subsequence.isEmpty();
    }
}
